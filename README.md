# Fortunella Cover

I made this cover in April 2018 for the album *Fortunella* by the band [Drunken Angel][drunken-angel]; consisting of [Tom Müller][tom] and Sophia Burtscher.

![Fortunella Cover.](./sizes/fortunella-cover-1280.jpg)

 The cover is licensed under a [CC BY-ND 4.0 License][cc4nd].
 
 The work is a derivative of “[A lemon plant (Citrus japonica)][original]”, 1876, by Walter Hood Fitch, licensed under a [CC BY 4.0 License][cc4] by the [Wellcome Collection][wellcome].
 
 The letters were made by [Adrian Frutiger][frutiger] as part of his commercial typeface Président (available from [Linotype][linotype]).

[original]: https://wellcomecollection.org/works/bxrevzru
[cc4]: https://creativecommons.org/licenses/by/4.0/
[cc4nd]: https://creativecommons.org/licenses/by-nd/4.0/
[wellcome]: https://en.m.wikipedia.org/wiki/Wellcome_Collection
[linotype]: https://www.linotype.com/de/49174/president-schriftfamilie.html
[frutiger]: https://en.m.wikipedia.org/wiki/Adrian_Frutiger

[tom]: //tommueller.eu/
[drunken-angel]: https://drunkenangelx.bandcamp.com/
